# Aula01 - Adição e Subtração

Fonte: [Ferretto YouTube](https://www.youtube.com/watch?v=e78_5WIssSU&list=PLTPg64KdGgYhYpS5nXdFgdqEZDOS5lARB&index=1)

## Números Decimais

## Adição

### Comparação de valores

![fig1](2022-04-27-12-20-04.png)

### Adição de números inteiros e números decimais

![fig2](2022-04-27-12-23-55.png)

### Propriedades da Adição

![fig3](2022-04-27-12-28-00.png)

## Subtração

### Operação de substração entre números inteiros

![fig4](2022-04-27-12-30-57.png)

### Operação de subtração entre números decimais

![fig5](2022-04-27-12-33-38.png)

## Notas

![fig6](2022-04-27-12-36-13.png)

Obs: no exemplo2 da imagem acima, temos: a - (b + c), onde o sinal de subtração inverte os "sinais" e não as operações, como segue:
8 + 3 - (4 - 9), onde ficará: 11 - ((+4) - (+9)), portanto:
11 - 4 + 9 = 16 //
