# Aula02 - Multiplicação e Divisão

Fonte: [Ferreto YouTube](https://www.youtube.com/watch?v=0UGJRHq2PS4&list=PLTPg64KdGgYhYpS5nXdFgdqEZDOS5lARB&index=2)

Exercícios: [Matemática Básica dot Net](https://matematicabasica.net/exercicios-de-subtracao/)

## Multiplicação

### Regra de sinais na Multiplicação

![fig1](2022-04-27-12-48-01.png)

### Multiplicação entre números inteiros

![fig2](2022-04-27-12-50-31.png)

### Multiplicação entre decimais

![fig3](2022-04-27-12-54-10.png)

### Propriedades da multiplicação

![fig4](2022-04-27-12-58-18.png)

#### Chuveirinho

![fi4a](2022-04-27-12-59-23.png)

## Divisão

### Regra de sinais na Divisão

![fig5](2022-04-27-13-04-26.png)

### Operação de divisão entre números inteiros

![fig6](2022-04-27-13-08-25.png)

#### Divisão inexata

![fig7](2022-04-27-13-12-01.png)

### Operação de divisão entre números inteiros com resultado decimal

![fig8](2022-04-27-13-15-55.png)

![fig8a](2022-04-27-13-19-47.png)

![fig8b](2022-04-27-13-22-28.png)

### Operação de divisão entre números decimais

![fig9](2022-04-27-13-25-23.png)

![fig10](2022-04-27-14-46-16.png)
